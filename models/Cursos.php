<?php
class Cursos extends model{
	
	public function getAll(){
		$array = array();

		$sql = "SELECT * FROM cursos";
		$sql = $this->db->query($sql);

		if ($sql->rowCount()>0) {
			$array = $sql->fetchAll();
		}

		return $array;
	}

	public function get($id){
		$array = array();
		$sql = "SELECT * FROM cursos WHERE id = :id";
		$sql = $this->db->prepare($sql);
		$sql->bindValue(":id", $id);
		$sql->execute();

		if ($sql->rowCount() > 0) {
			$array = $sql->fetch();
		} else {

		}
		
		return $array;
	}

	public function add($nome, $id_professor){
		$sql = $this->db->prepare("INSERT INTO cursos SET nome = :nome, criacao = NOW(), id_professor = :id_professor");
		$sql->bindValue(":nome", $nome);
		$sql->bindValue(":id_professor", $id_professor);
		$sql->execute();
	}

	public function edit($nome, $id_professor, $id){
        $sql = $this->db->prepare("UPDATE cursos SET nome = :nome,  id_professor = :id_professor WHERE id = :id");
        $sql->bindValue(":nome", $nome);
        $sql->bindValue(":id_professor", $id_professor);
        $sql->bindValue(":id", $id);
        $sql->execute();
    }


	public function delete($id){
		$sql = $this->db->prepare("DELETE FROM cursos WHERE id = :id");
		$sql->bindValue(":id", $id);
		$sql->execute();
	}
	
}






