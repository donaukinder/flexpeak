<?php
class Alunos extends model{
	
	public function getAll(){
		$array = array();

		$sql = "SELECT * FROM alunos";
		$sql = $this->db->query($sql);

		if ($sql->rowCount()>0) {
			$array = $sql->fetchAll();
		}

		return $array;
	}

    public function get($id){
        $array = array();
        $sql = "SELECT * FROM alunos WHERE id = :id";
        $sql = $this->db->prepare($sql);
        $sql->bindValue(":id", $id);
        $sql->execute();

        if ($sql->rowCount() > 0) {
            $array = $sql->fetch();
        } else {

        }
        
        return $array;
    }
    
	public function add($nome, $data_nascimento, $logradouro, $numero, $bairro, $cidade, $cep, $curso){
        
        $sql = $this->db->prepare ("INSERT INTO alunos (nome, data_nascimento, logradouro, numero, bairro, cidade, data_criacao, cep, id_curso) VALUES (:nome, :data_nascimento, :logradouro, :numero, :bairro, :cidade, NOW(), :cep, :id_curso)");
        $sql->bindValue(":nome", $nome);
        $sql->bindValue(":data_nascimento", $data_nascimento);
        $sql->bindValue(":logradouro", $logradouro);
        $sql->bindValue(":numero", $numero);
        $sql->bindValue(":bairro", $bairro);
        $sql->bindValue(":cidade", $cidade);
        $sql->bindValue(":cep", $cep);
        $sql->bindValue(":id_curso", $curso);
        $sql->execute();    
	}

    public function edit($nome, $data_nascimento, $logradouro, $numero, $bairro, $cidade, $cep, $curso, $id){
             $sql = $this->db->prepare("UPDATE alunos SET nome = :nome,  data_nascimento = :data_nascimento, logradouro = :logradouro, numero = :numero, bairro = :bairro, cidade = :cidade, cep = :cep, id_curso = :id_curso WHERE id = :id");
        $sql->bindValue(":nome", $nome);
        $sql->bindValue(":data_nascimento", $data_nascimento);
        $sql->bindValue(":logradouro", $logradouro);
        $sql->bindValue(":numero", $numero);
        $sql->bindValue(":bairro", $bairro);
        $sql->bindValue(":cidade", $cidade);
        $sql->bindValue(":cep", $cep);
        $sql->bindValue(":id_curso", $curso);
        $sql->bindValue(":id", $id);
        $sql->execute();
               
    }

    public function delete($id){
        $sql = $this->db->prepare("DELETE FROM alunos WHERE id = :id");
        $sql->bindValue(":id", $id);
        $sql->execute();
    }
}





