<?php
class Relatorio extends model{

	public function get(){
		$array = array();

		$sql = "SELECT A.nome as nomeAluno, C.nome as nomeCurso, P.nome as nomeProfessor FROM alunos A INNER JOIN cursos C ON A.id_curso = C.id INNER JOIN professores P ON C.id_professor = P.id";
		
		$sql = $this->db->query($sql);
		if ($sql->rowCount()>0) {
			$array = $sql->fetchAll();
		}
		return $array;
	}
}