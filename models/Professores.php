<?php
class Professores extends model{
	
	public function getAll(){
		$array = array();

		$sql = "SELECT * FROM professores";
		$sql = $this->db->query($sql);

		if ($sql->rowCount()>0) {
			$array = $sql->fetchAll();
		}

		return $array;
	}

	public function get($id){
		$array = array();
		$sql = "SELECT * FROM professores WHERE id = :id";
		$sql = $this->db->prepare($sql);
		$sql->bindValue(":id", $id);
		$sql->execute();

		if ($sql->rowCount() > 0) {
			$array = $sql->fetch();
		}else {

		}	
		return $array;
	}
	
	public function add($nome, $data_nascimento){
		$sql = $this->db->prepare("INSERT INTO professores SET nome = :nome, data_nascimento = :data_nascimento, data_criacao = NOW()");
		$sql->bindValue(":nome", $nome);
		$sql->bindValue(":data_nascimento", $data_nascimento);
		$sql->execute();
	}

	public function edit($nome, $data_nascimento, $id){
        $sql = $this->db->prepare("UPDATE professores SET nome = :nome, data_nascimento = :data_nascimento WHERE id = :id");
		$sql->bindValue(":nome", $nome);
		$sql->bindValue(":data_nascimento", $data_nascimento);
		$sql->bindValue(":id", $id);
		$sql->execute();
    }

	public function delete($id){
		$sql = $this->db->prepare("DELETE FROM professores WHERE id = :id");
		$sql->bindValue(":id", $id);
		$sql->execute();
	}
	
}












