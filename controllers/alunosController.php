<?php
class alunosController extends controller{
	
	public function index(){}


	public function add(){
		$data = array();

		$a = new Alunos();
		if (isset($_POST['nome']) && !empty($_POST['nome'])) {
			$nome = addslashes($_POST['nome']);
			$data_nascimento = addslashes($_POST['data_nascimento']);
			$logradouro = addslashes($_POST['logradouro']);
			$numero = addslashes($_POST['numero']);
			$bairro = addslashes($_POST['bairro']);
			$cidade = addslashes($_POST['cidade']);
			$cep = addslashes($_POST['cep']);
			$curso = addslashes($_POST['curso']);


			$a->add($nome, $data_nascimento, $logradouro, $numero, $bairro, $cidade, $cep, $curso);
			header("location: ".BASE_URL);

		}
		$this->loadTemplate('add_aluno', $data);
	}

	public function edit($id){
        $data = array();
 		
	        if (!empty($id)) {
	        	$alunos = new Alunos();

	        	if (!empty($_POST['nome'])) {
	        		$nome = $_POST['nome'];
	        		$data_nascimento = $_POST['data_nascimento'];
	        		$logradouro = $_POST['logradouro'];
	        		$numero = $_POST['numero'];
	        		$bairro = $_POST['bairro'];
	        		$cidade = $_POST['cidade'];
	        		$cep = $_POST['cep'];
	        		$curso = $_POST['curso'];

	        		$alunos->edit($nome, $data_nascimento, $logradouro, $numero, $bairro, $cidade, $cep, $curso, $id);
	        	}else {
	                $data['info'] = $alunos->get($id);
	            	if (isset($data['info']['id'])) {
	                	$this->loadTemplate('edit_aluno', $data);
	               	exit;
	            }
	        }	
	    }
        header("location: ".BASE_URL);       
    }

	 public function delete($id){

        $data = array();
        $a = new Alunos();
       
        $a->delete($id);
        header("location: ".BASE_URL);
        
    }
}
