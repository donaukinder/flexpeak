<?php
class cursosController extends controller {

	public function index() {
		$dados = array();

		$cursos = new Cursos();

		$dados['lista'] = $cursos->getAll();

		$this->loadTemplate('lista_cursos', $dados);

	}

	public function add(){
		$data = array();

		$cursos = new Cursos();
		if (isset($_POST['nome']) && !empty($_POST['nome'])) {
			$nome = addslashes($_POST['nome']);
			$id_professor = addslashes($_POST['id_professor']);


			$cursos->add($nome, $id_professor);
			header("location: ".BASE_URL."cursos/");

		}

		$this->loadTemplate('add_curso', $data);
	}

	public function edit($id){
        $data = array();
 		
	        if (!empty($id)) {
	        	$cursos = new Cursos();

	        	if (!empty($_POST['nome'])) {
	        		$nome = $_POST['nome'];
	        		$id_professor = $_POST['id_professor'];

	        		$cursos->edit($nome, $id_professor, $id);
	        	}else {
	                $data['info'] = $cursos->get($id);
	            	if (isset($data['info']['id'])) {
	                	$this->loadTemplate('edit_curso', $data);
	               	exit;
	            }
	        }	
	    }
        header("location: ".BASE_URL."cursos/");       
    }

    public function delete($id){

        $data = array();
        $cursos = new Cursos();
       
        $cursos->delete($id);
        header("location: ".BASE_URL."cursos/");
        
    }
}