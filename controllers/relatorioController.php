<?php
class relatorioController extends controller {

	public function index() {
		$dados = array();

		$info = new Relatorio();
		
		$dados['lista'] = $info->get();

		$this->loadView('relatorio', $dados);

	}
}