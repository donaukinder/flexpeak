<?php
class homeController extends controller {

	public function index() {
		$dados = array();

		$alunos = new Alunos();

		$dados['lista'] = $alunos->getAll();

		$this->loadTemplate('home', $dados);

	}
}