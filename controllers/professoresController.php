<?php
class professoresController extends controller {

	public function index() {
		$dados = array();

		$professores = new Professores();

		$dados['lista'] = $professores->getAll();

		$this->loadTemplate('lista_professores', $dados);

	}

	public function add(){
		$data = array();

		$p = new Professores();
		if (isset($_POST['nome']) && !empty($_POST['nome'])) {
			$nome = addslashes($_POST['nome']);
			$data_nascimento = addslashes($_POST['data_nascimento']);


			$p->add($nome, $data_nascimento);
			header("location: ".BASE_URL."professores/");

		}

		$this->loadTemplate('add_professor', $data);
	}
	

	public function edit($id){
        $data = array();
 		
	        if (!empty($id)) {
	        	$professores = new Professores();

	        	if (!empty($_POST['nome'])) {
	        		$nome = $_POST['nome'];
	        		$data_nascimento = $_POST['data_nascimento'];

	        		$professores->edit($nome, $data_nascimento, $id);
	        	}else {
	                $data['info'] = $professores->get($id);
	            	if (isset($data['info']['id'])) {
	                	$this->loadTemplate('edit_professor', $data);
	               	exit;
	            }
	        }	
	    }
        header("location: ".BASE_URL."professores/");       
    }

	public function delete($id){

        $data = array();
        $p = new Professores();
       
        $p->delete($id);
        header("location: ".BASE_URL."professores/");
        
    }

}