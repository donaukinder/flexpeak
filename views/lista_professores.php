<div class="container" align="center">
<h1>Professores</h1>
<a href="<?php echo BASE_URL;?>professores/add" class="btn btn-primary mt-2 mb-2">ADICIONAR</a>
<table class="table table-bordered">
	<thead class="thead-dark">
	<tr align="center">
		<th>ID</th>
		<th>Nome</th>
		<th>Data de Nascimento</th>
		<th>Data da criação</th>
		<th>AÇÕES</th>
	</tr>
	</thead>
	<?php foreach ($lista as $item):?>
		<tr align="center">
			<td><?php echo $item['id'];?></td>
			<td><?php echo $item['nome'];?></td>
			<td><?php echo $item['data_nascimento'];?></td>
			<td><?php echo $item['data_criacao'];?></td>
			<td>
				<a class="btn btn-secondary btn-sm" href="<?php echo BASE_URL; ?>professores/edit/<?php echo $item['id'];?>">editar</a>
				<a class="btn btn-danger btn-sm" href="<?php echo BASE_URL;?>professores/delete/<?php echo $item['id'];?>" onclick="return confirm('Realmente deseja excluir?')">Excluir</a>
			</td>
		</tr>
	<?php endforeach; ?>	
</table>
</div>