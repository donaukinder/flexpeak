<div class="container">
		<h1>Alunos - Adicionar</h1>

		<form method="POST">
		<div class="form-group">
	    	<label for="nome">Nome:</label>
	    	<input type="text" class="form-control" name="nome" required>
  		</div>	
		<div class="form-group">
	    	<label for="data_nascimento">Data de Nascimento:</label>
	    	<input type="text" class="form-control" name="data_nascimento" required>
  		</div>
    	<div class="form-group">
	    	<label for="cep">CEP:</label>
	    	<input type="text" class="form-control" name="cep" required>
    	</div>
    	<div class="form-group">
	    	<label for="logradouro">Rua:</label>
	    	<input type="text" class="form-control" name="logradouro" required>
    	</div>
    	<div class="form-group">
	    	<label for="numero">Número:</label>
	    	<input type="text" class="form-control" name="numero" required>
    	</div>
    	<div class="form-group">
	    	<label for="bairro">Bairro:</label>
	    	<input type="text" class="form-control" name="bairro" required>
    	</div>
    	<div class="form-group">
	    	<label for="cidade">Cidade:</label>
	    	<input type="text" class="form-control" name="cidade" required>
    	</div>
    	<div class="form-group">
	    	<label for="curso">Curso:</label>
	    	<input type="text" class="form-control" name="curso" required>
    	</div>
    	<input type="submit" value="Adicionar" class="btn btn-success mt-3 mb-5">
		</form>
	</div>
	

