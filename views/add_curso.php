<div class="container">
		<h1>Cursos - Adicionar</h1>

		<form method="POST">
		<div class="form-group">
	    	<label for="nome">Nome:</label>
	    	<input type="text" class="form-control" name="nome" required>
  		</div>
    	<div class="form-group">
	    	<label for="id_professor">ID Professor:</label>
	    	<input type="text" class="form-control" name="id_professor" required>
    	</div>
    	<input type="submit" value="Adicionar" class="btn btn-success mt-3 mb-5">
		</form>
	</div>
