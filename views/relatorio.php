<?php 
ob_start();
require 'vendor/autoload.php';
?>
<div align="center">
<h1 align="center">Relatório</h1>
<table border="1" width="100%">
	<thead>
	<tr align="center">
		<th>Nome do Aluno</th>
		<th>Curso</th>
		<th>Professor</th>
	</tr>
	</thead>
	<?php foreach ($lista as $item):?>
		<tr>
			<td align="center"><?php echo $item['nomeAluno'];?></td>
			<td align="center"><?php echo $item['nomeCurso'];?></td>
			<td align="center"><?php echo $item['nomeProfessor'];?></td>
		</tr>
	<?php endforeach; ?>
</table>
</div>
<?php
$html = ob_get_contents();
ob_end_clean();
$mpdf = new \Mpdf\Mpdf();
$mpdf->WriteHTML($html);
$mpdf->Output();
