<html>
	<head>
		<title>CRUD - FLEXPEAK</title>
	</head>
	<link rel="stylesheet" href="<?php echo BASE_URL;?>assets/css/bootstrap.min.css">
	<body>
		<nav class="navbar navbar-expand-lg navbar-light bg-light">
		  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Alterna navegação">
		    <span class="navbar-toggler-icon"></span>
		  </button>
		  <div class="collapse navbar-collapse" id="navbarNav">
		    <ul class="navbar-nav">
		      <li class="nav-item">
		        <a class="nav-link" href="<?php echo BASE_URL;?>">Alunos<span class="sr-only">(Página atual)</span></a>
		      </li>
		      <li class="nav-item">
		        <a class="nav-link" href="<?php echo BASE_URL;?>cursos/">Cursos</a>
		      </li>
		      <li class="nav-item">
		        <a class="nav-link" href="<?php echo BASE_URL;?>professores/">Professores</a>
		      </li>
		      <li>
		      	<a class="btn btn-secondary" target="_blank" href="<?php echo BASE_URL;?>relatorio/">Relatório</a>
		      </li>
		    </ul>
		  </div>
	</nav>
		<?php $this->loadViewInTemplate($viewName, $viewData);?>
	</body>
	<script src="<?php echo BASE_URL;?>assets/js/jquery.min.js"></script>
	<script src="<?php echo BASE_URL;?>assets/js/jquery.mask.min.js"></script>
	<script src="<?php echo BASE_URL;?>assets/js/scripts.js"></script>
	<script src="<?php echo BASE_URL;?>assets/js/popper.min.js"></script>
	<script src="<?php echo BASE_URL;?>assets/js/bootstrap.min.js"></script>
</html>