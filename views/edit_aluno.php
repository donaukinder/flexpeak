<div class="container">
		<h1>Alunos - Adicionar</h1>

		<form method="POST">
		<div class="form-group">
	    	<label for="nome">Nome:</label>
	    	<input type="text" class="form-control" name="nome" value="<?php echo $info['nome']; ?>">
  		</div>	
		<div class="form-group">
	    	<label for="data_nascimento">Data de Nascimento:</label>
	    	<input type="text" class="form-control" name="data_nascimento" value="<?php echo $info['data_nascimento']; ?>">
  		</div>
    	<div class="form-group">
	    	<label for="cep">CEP:</label>
	    	<input type="text" class="form-control" name="cep" value="<?php echo $info['cep']; ?>">
    	</div>
    	<div class="form-group">
	    	<label for="logradouro">Rua:</label>
	    	<input type="text" class="form-control" name="logradouro" value="<?php echo $info['logradouro'];?>">
    	</div>
    	<div class="form-group">
	    	<label for="numero">Número:</label>
	    	<input type="text" class="form-control" name="numero" value="<?php echo $info['numero']; ?>">
    	</div>
    	<div class="form-group">
	    	<label for="bairro">Bairro:</label>
	    	<input type="text" class="form-control" name="bairro" value="<?php echo $info['bairro']; ?>">
    	</div>
    	<div class="form-group">
	    	<label for="cidade">Cidade:</label>
	    	<input type="text" class="form-control" name="cidade" value="<?php echo $info['cidade']; ?>">
    	</div>
    	<div class="form-group">
	    	<label for="curso">Curso:</label>
	    	<input type="text" class="form-control" name="curso" value="<?php echo $info['id_curso']; ?>">
    	</div>
    	<input type="submit" value="Salvar" class="btn btn-success mt-3 mb-5">
		</form>
	</div>
	

