<div class="container" align="center">
	<h1>Cursos</h1>
<a href="<?php echo BASE_URL;?>cursos/add" class="btn btn-primary mt-2 mb-2">ADICIONAR</a>
<table class="table table-bordered">
	<thead class="thead-dark">
	<tr align="center">
		<th>ID</th>
		<th>Curso</th>
		<th>Data de Criação</th>
		<th>ID_Professor</th>
		<th>AÇÕES</th>
	</tr>
	</thead>
	<?php foreach ($lista as $item):?>
		<tr align="center">
			<td><?php echo $item['id'];?></td>
			<td><?php echo $item['nome'];?></td>
			<td><?php echo $item['criacao'];?></td>
			<td><?php echo $item['id_professor'];?></td>
			<td>
				<a class="btn btn-secondary btn-sm" href="<?php echo BASE_URL; ?>cursos/edit/<?php echo $item['id'];?>">editar</a>
				<a class="btn btn-danger btn-sm" href="<?php echo BASE_URL;?>cursos/delete/<?php echo $item['id'];?>" onclick="return confirm('Realmente deseja excluir?')">Deletar</a>
			</td>
		</tr>
	<?php endforeach; ?>	
</table>
</div>