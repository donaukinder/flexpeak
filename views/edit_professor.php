<div class="container">
		<h1>Editar - Professor</h1>

		<form method="POST">
		<div class="form-group">
	    	<label for="nome">Nome:</label>
	    	<input type="text" class="form-control" name="nome" value="<?php echo $info['nome']; ?>">
  		</div>
    	<div class="form-group">
	    	<label for="data_nascimento">Data nascimento:</label>
	    	<input type="text" class="form-control" name="data_nascimento" value="<?php echo $info['data_nascimento']; ?>">
    	</div>
    	<input type="submit" value="Salvar" class="btn btn-success mt-3 mb-5">
		</form>
	</div>
