$(function(){
	$('input[name=cep]').mask('99999-999', {placeholder:"00000-000"});
	$('input[name=data_criacao]').mask('00-00-0000', {placeholder:"00-00-0000"});
	$('input[name=data_nascimento]').mask('00-00-0000', { placeholder:"00-00-0000"});
});

$('input[name=cep]').on('blur', function(){
	var cep = $(this).val();

	$.ajax({
		url:'https://api.postmon.com.br/v1/cep/'+cep,
		type:'GET',
		dataType:'json',
		success:function(json){
			if (typeof json.logradouro != 'undefined') {
				$('input[name=logradouro]').val(json.logradouro);
				$('input[name=bairro]').val(json.bairro);
				$('input[name=cidade]').val(json.cidade);
				$('input[name=estado]').val(json.estado);
				$('input[name=numero]').focus();
			}
		}
	});
});
